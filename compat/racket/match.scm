(define-module (compat racket match)
  #:use-module (compat racket match match)
  #:export (match)
  #:re-export (match* define-match-expander
		      match/values match-lambda match-lambda* match-lambda**
		      match-let match-let-values match-let*-values
		      match-letrec 
		      match-define match-define-values))

(define-syntax-rule (match . l) ((@ (compat racket match match) match) . l))

