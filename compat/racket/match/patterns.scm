(define-module (compat racket match patterns)
  #:use-module (compat racket match match)
  #:re-export (pat lvp lvq mid qp literal it match-cl))
